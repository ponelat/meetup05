var express = require('express');
var path = require('path');
var app = express();

var port = process.env.PORT || 80

app.use('/', (req,res) => {
  res.send('Server One')
});

const server = app.listen(port, () => {
  const { hostname, port } = server.address()
  console.log(`Listening on ${hostname}:${port}`)
})
